const electron = require('electron')
var uuid = require('node-uuid');
const window = require('electron-window')
var argv = require('minimist')(process.argv.slice(2))
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindows = []
let numberOfWindows = argv._[0] ? parseInt(argv._[0], 10) : 1;
let partition
let urlTarget = argv._[1] ? argv._[1] : "http://google.com";
let enableDev = argv._[2] ? Boolean(argv._[2]) : false;

function createWindow () {
  for (i = 0; i < numberOfWindows; i++) {
    // Create the browser window.
    let newWindow = window.createWindow({width: 800, height: 600})//new BrowserWindow({width: 800, height: 600, show: false})
    partition = uuid.v1()
    console.log(newWindow.id);
    // window.id = partition;

    let html = [
      '<html>',
        '<style>',
          'webview {',
            'min-width:800px;',
            'min-height:600px;',
          '}',
          'a { cursor: pointer; }',
          '.header { margin-bottom: 20px; padding: 10px; }',
        '</style>',
        '<body>',
          '<div class="header"><a style="float: right" id="reload">Reload</a></div>',
          '<webview id="webview" src="' + urlTarget + '" partition="' + partition + '"></webview>',
          '<script>',
            'window.onload = function() {',
              'document.getElementById("reload").addEventListener("click", function() { ',
                'document.getElementById("webview").reload();',
              '})',
            '}',
          '</script>',
        '</body>',
      '</html>'
    ].join('');

    newWindow.showURL("data:text/html;charset=utf-8," + encodeURI(html));

    // Open the DevTools.
    if (enableDev) {
      newWindow.webContents.openDevTools()
    }
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (isEmptyObject(window.windows)) {
    createWindow()
  }
})

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }

  return true;
}

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
